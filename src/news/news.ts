import dbConnent from '../database/connection'


export default class News extends dbConnent {
  
  getAllNews(start: number, offset: number) {
    return this.dbQuery('select news.nid, news.ndatecreate, news.nphoto, news.ntitle, news.ndiscription from news order by news.ndatecreate desc, nid offset $1 limit $2 ', [offset, start]);
  }


  countNews() {
    return this.dbQuery('select count(*) from news');
  }

  getNewsById(id:number) {
    return this.dbQuery('select * from news where nid = $1', [id]);
  }

  delNews(id:number) {
    return this.dbQuery('delete from news where nid = $1', [id]);
  }

  addNews(date: string, title: string, discription: string, image: string, template: string, user_id: number) {
    return this.dbQuery('insert into news(ndatecreate, ntitle, nphoto, ntemplate, u_id, ndiscription) values ($1, $2, $3, $4, $5, $6)', [date, title, image, template, user_id, discription]);
  }

}