import nodemailer from 'nodemailer';

export default async function mail(email: any, title: any, text: any) {
  let testAccount = await nodemailer.createTestAccount();

  let transporter = nodemailer.createTransport({
    host: 'smtp.mail.ru',
    port: 587,
    secure: false,
    auth: {
      user: 'name@mail.ru',
      pass: 'pass',
    },
  });

  let info = await transporter.sendMail({
    from: '"AndersenLife" <vkepels@mail.ru>',
    to: `${email}`,
    subject: `${title}`,
    text: `${text}`,
    html: `<p>${text}</p>`,
  });

  console.log('Message sent: %s', info.messageId);

  console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
}
