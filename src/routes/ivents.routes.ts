import express = require('express');
import upload from '../storage.setting';
import Ivents from '../events/ivents'; 
//@ts-ignore
import cors from 'cors'
const app = express();
const ivents = new Ivents();

import mail from '../email/email';
import User from './../users/users'

const user = new User();


app.get('/all/:start/:offset', function (req, res) {
  ivents.countIvent()
  .then((count: []) => {
    ivents.getAllIvents(req.params.start, req.params.offset)
    .then((data: any) => {
      // data.push(count);
      res.send(data);
    });
  })
});

app.get('/get/:id', function (req, res) {
  ivents.getIventById(req.params.id)
    .then((data: any) => {
      res.json(data[0]);
    });
});

const arr = upload.fields([ {name: "image"}])
app.post('/arr', arr, cors(), (req: any, res: any) => {
  console.log(req.files);
  res.json('sds');


})


const add = upload.fields([{name: 'datestart'}, {name: 'dateend'}, {name: 'title'}, {name: 'description'}, {name: "image"}, {name: 'count'}, {name: 'id'}])
app.post('/add', add, cors(), (req: any, res: any) => {
  
  ivents.addIvent(req.body.datestart, req.body.dateend, req.body.title, req.body.description, req.files['image'][0].filename, req.body.count, req.body.id)
  .then((data: any) => {
    console.log(data);
    
    if (data.length == 0) {
      user.getEmail()
      .then((data:any) => {
            
        let ml:any = '';
        for (let i = 0; i < data.length; i++) {
        ml += `${data[i].umail}, `;      
        }
        // console.log(ml);
        // console.log(req.body.title);
        // console.log(req.body.discription);
        
        mail(ml, `Добавлено событие - ${req.body.title}`, req.body.description).catch(console.error);
        res.send(["ok"]);

      })
    } else {
      res.send(data);
    }
  })
})

app.get('/party/:id', (req: any, res:any) => {
  console.log(req.params.id);
  
  ivents.partyIvent(req.params.id)
  .then((data: any) => {
    res.json(data);
  })
})

app.get('/join/:i_id/:u_id', (req:any, res:any) => {
  ivents.join(req.params.i_id, req.params.u_id)
  .then((data: any) => {
    if (data.length == 0) {
      res.json("ok");
    } else {
      res.json(data);
    }
  })
})




export default app;
