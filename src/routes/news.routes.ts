import express = require('express');
import upload from './../storage.setting';
import News from './../news/news';
import User from './../users/users'
//@ts-ignore
import cors from 'cors'
const app = express();

import mail from '../email/email';

const news = new News();
const user = new User();

app.get('/all/:start/:offset', (req:any, res:any) => {
  news.countNews()
  .then((count: []) => {
    news.getAllNews(req.params.start, req.params.offset)
    .then((data:any) => {
      // data.push(count);
      res.send(data);
    })
  });
});


app.get('/em', (req: any, res:any) => {
  user.getEmail()
  .then((data:any) => {
    console.log(data.length);
    
    let ml:any = '';
    for (let i = 0; i < data.length; i++) {
     ml += `${data[i].umail}, `;      
    }
    console.log(ml);
    res.json(ml);

    
    mail(ml, 'pip', 'pop').catch(console.error);
    
  })
})

app.get('/get/:id', cors(), (req: any, res: any) => {
  news.getNewsById(req.params.id)
  .then((data:any) => {
    res.send(data[0]);
  })
})

app.get('/delete/:id', (req: any, res: any) => {
  news.delNews(req.params.id)
  .then((data:any) => {
    if (data.length == 0) {
      res.send("ok");
    } else {
      res.send(data);
    }
  })
})




const add = upload.fields([{ name: 'date'}, {name: 'title'}, { name: 'discription'}, { name: 'image'}, { name: 'template'}, {name: 'user_id'}]);
app.post('/add', add, cors(), (req: any, res: any, ) => {

  
  news.addNews(req.body.date, req.body.title, req.body.discription, req.files['image'][0].filename, req.body.template, req.body.user_id)
  .then((data:any) => {
    if (data.length == 0) {
      user.getEmail()
      .then((data:any) => {
            
        let ml:any = '';
        for (let i = 0; i < data.length; i++) {
        ml += `${data[i].umail}, `;      
        }
        // console.log(ml);
        // console.log(req.body.title);
        // console.log(req.body.discription);
        
        mail(ml, `Добавлена новость - ${req.body.title}`, req.body.discription).catch(console.error);
        res.send(["ok"]);

      })
    } else {
      res.send(data);
    }
  })
})


export default app;