const axios = require('axios');

import ajax from 'ajax'


const _url = `http://192.168.43.70:3000`;


export function addNews(data) {

  // console.log(data.getAll('image'));

  return fetch(`${_url}/news/add`,{
    method: 'post',
    body: data,
  }).then(res => res.json())

}


export function getNewsById(id) {
  return select(`news/get/${id}`)
}

export function getUserById(id) {
  return select(`user/get/${id}`)
}


function select(call, body = {option : 'get'}) {
  const url = `${_url}/${call}`;
  return fetch(url, {
    ...body,
  }).then(res => res.json())
    .catch(er => {
      console.log("error", er)
    })
    .then(data => {
      return data;
    })

}