import Vue from 'vue'
import Router from 'vue-router'
import News from './views/News.vue'
import Article from './views/Article.vue'
Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/:id',
      name: 'news',
      component: News,
      props: true
    },
    {
      path: '/article/:id',
      name: 'article',
      component: Article,
      props: true
    }
  ]
})
